db.fruits.aggregate([
  
  {$match: {supplier:"Red Farms Inc."}},
  {$count: "itemsRedFarms"}
    
])
  
db.fruits.aggregate([
  
  {$match: {price:{$gt: 50}}},
  {$count: "priceGreaterThan50"}
    
])
  
db.fruits.aggregate([

    {$match: {onSale:true}},
    {$group: {_id:"$supplier",avgPricePerSupplier:{$avg:"$price"}}}

])

db.fruits.aggregate([

    {$match: {onSale:true}},
    {$group: {_id:"$supplier",maxPricePerSupplier:{$max:"$price"}}}

])
    
db.fruits.aggregate([

    {$match: {onSale:true}},
    {$group: {_id:"$supplier",minPricePerSupplier:{$min:"$price"}}}

])
